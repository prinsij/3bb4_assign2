import java.util.ArrayList;

class Baboon extends Thread {
	private BaboonController control;
	private boolean direction;
	private boolean crossing = false;
	private static final ArrayList<Baboon> allBaboons = new ArrayList<Baboon>();

	public Baboon(BaboonController control, boolean direction) {
		this.control = control;
		this.direction = direction;
		allBaboons.add(this);
	}

	public void run() {
		System.out.println("new Baboon, " + direction);
		while (true) {
			if (control.tryEnter(direction)) {
				crossing = true;
				cross();
				crossing = false;
				control.exit();
				break;
			}
			try {
				sleep(100);
			} catch (InterruptedException e) {
				System.out.println("interrupted nap");
			}
		}
	}

	private void cross() {
		for (Baboon b : allBaboons) {
			assert(!b.crossing || b.direction == direction);
		}
		try {
			sleep(400);
		} catch (InterruptedException e) {
			System.out.println("interrupted nap");
		}
	}
}

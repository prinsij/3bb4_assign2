import java.util.Random;

class Main {
	static final int NUM_BABOONS = 100;
	public static void main(String[] args) {
		BaboonController control = new BaboonController();
		Random rand = new Random();
		control.start();
		for (int i=0; i < NUM_BABOONS; i++) {
			new Baboon(control, rand.nextBoolean()).start();
		}
	}
}

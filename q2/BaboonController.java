class BaboonController extends Thread {
	private static final int MAX_CROSS = 5;
	private static final int SWITCH_RATE = 30;
	
	private int numCrossing = 0;
	private boolean direction = false;
	
	private int numCrossed = 0;
	private boolean blocked = false;

	synchronized boolean tryEnter(boolean Edirection) {
		if (blocked) {
			return false;
		}
		if (direction == Edirection && numCrossing < MAX_CROSS) {
			numCrossing += 1;
			System.out.println("enter " + Edirection);
			return true;
		} else if (numCrossing == 0) {
			numCrossed = 0;
			numCrossing = 1;
			direction = Edirection;
			System.out.println("enter " + Edirection);
			return true;
		}
		//System.out.println("rejected " + Edirection);
		return false;
	}

	synchronized void exit() {
		System.out.println("exit " + direction);
		numCrossing -= 1;
		if (numCrossing == 0) {
			blocked = false;
		}
		numCrossed += 1;
		if (numCrossed == SWITCH_RATE) {
			System.out.println("switch");
			numCrossed = 0;
			direction = !direction;
			if (numCrossing > 0) {
				blocked = true;
			}
		}
	}
}
